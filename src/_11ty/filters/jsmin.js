// compress and combine js files
const UglifyJS = require("uglify-js");

module.exports = (code) => {
	let minified = UglifyJS.minify(code);
	if (minified.error) {
		console.log("UglifyJS error: ", minified.error);
		return code;
	}
	return minified.code;
};
