// JSON-LD escape special characters

module.exports = (obj) => {
	const json = JSON.stringify(obj);
	const jsonesc = JSON.stringify(json);
	const escstring = JSON.parse(jsonesc);
	return escstring;
};
