// required packages
//const fetch = require("node-fetch");
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));

// get Objave
async function getItems() {

	// Objave array
	let allitems = [];

	try {
		// initiate fetch
		const graphcms = await fetch("https://api-eu-central-1.graphcms.com/v2/cky8dxecn2p0001z4b6fn0dhc/master", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
			},
			body: JSON.stringify({
				query: `{
                    tags(stage: PUBLISHED) {
                        name
												slug
												post {
													title
													date
													slug
													coverImage {
														handle
														url
													}
													authors {
														name
													}
												}
                    }
                }`
			})
		});

		// store the JSON response when promise resolves
		const response = await graphcms.json();

		// handle DatoCMS errors
		if (response.errors) {
			let errors = response.errors;
			errors.map((error) => {
				console.log(error.message);
			});
			throw new Error("Aborting: GraphCMS errors");
		}

		// update blogpost array with the data from the JSON response
		allitems = allitems.concat(response.data.tags);

	} catch (error) {
		throw new Error(error);
	}

	
	
	// return formatted blogposts
	return allitems;
}

// export for 11ty
module.exports = getItems;