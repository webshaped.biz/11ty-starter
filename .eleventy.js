const del = require("del"); // deleting _site folder
const eleventyGoogleFonts = require("eleventy-google-fonts");

module.exports = function (config) {
  
  /* CONTEXT */
  let env = process.env.ELEVENTY_ENV;

  /* FILTERS */
  config.addFilter("squash", require("./src/_11ty/filters/squash.js") );
  config.addFilter("dateFormat", require("./src/_11ty/filters/date.js"));
  config.addFilter("section", require("./src/_11ty/filters/section.js"));
  config.addFilter("dump", require("./src/_11ty/filters/dump.js"));
  config.addFilter("debugger", require("./src/_11ty/filters/debugger.js"));
  config.addFilter("limit", require("./src/_11ty/filters/limit.js"));
  config.addFilter("jsonldesc", require("./src/_11ty/filters/jsonldesc.js"));
  config.addFilter("jsmin", require("./src/_11ty/filters/jsmin.js"));
  
  /* SHORTCODES */
  config.addShortcode("og_updated_time", () => new Date());
  config.addShortcode("year", () => `${new Date().getFullYear()}`);
  config.addShortcode('youtube',require('./src/_11ty/shortcodes/youtube'));

  /* PLUGINS */
  config.addPlugin(eleventyGoogleFonts);

  /* TRANSFORMS */
  // Minify the html output
  if (env != 'dev') {
    config.addTransform("htmlmin", require("./src/_11ty/utils/minify-html.js"));
  }

  /* PAGINATED COLLECTIONS */
  // Tags
  config.addCollection("pTags", async (collections) => {
    const getTags = require("./src/_data/tags_raw.js");
    const tags = await getTags();
    const taglist = Array.from(new Set(tags.flatMap((r) => r.name))).sort(); 
    console.log(taglist);
    return Array.from(new Set(tags.flatMap((r) => r.name))).sort();
  });
  
  /* CONFIGURATION */
  // Quiet mode
  if (env != 'dev') {
    config.setQuietMode(true);
  }
  // Delete _site before build
  const dirToClean = "_site/*";
  del(dirToClean);
  // Layout aliases can make templates more portable
  config.addLayoutAlias('home', 'layouts/home.njk');
  config.addLayoutAlias('page', 'layouts/page.njk');
  // pass some assets right through
  config.addPassthroughCopy("./src/assets");
  // make the seed target act like prod
  env = (env=="seed") ? "prod" : env;
  return {
    dir: {
      input: "src",
      output: "_site"
    },
    templateFormats : ["njk", "md", "11ty.js"],
    htmlTemplateEngine : "njk",
    markdownTemplateEngine : "njk",
    passthroughFileCopy: true
  };

};
